
from flask import Flask,jsonify
from dotenv import load_dotenv,dotenv_values
import cx_Oracle
from pyms.project import  services_
import multiprocessing
from pyms.project import commands_
import platform
import os
import sys
import logging
import traceback
from pyms.project.models.init_db import  sysConfig



print(os.getenv('ORACLE_HOME'))

def rabbitMQTask(cihazTipi):
    # Exchange ve kuyruk oluşturma

    exchange_name = sysConfig.get('MQ_EXCHANGE', r'test')
    print("consume:", cihazTipi,"exchange",exchange_name)
    commands_.create_exchange(exchange_name, exchange_type='topic')
    print("create_exchange->consume:", cihazTipi, "exchange", exchange_name)
    commands_.create_queue(cihazTipi)
    print("create_queue->consume:", cihazTipi, "exchange", exchange_name)
    commands_.bind_queue(exchange_name, cihazTipi, cihazTipi)  # Mesajları dinleme
    print("bind_queue->consume:", cihazTipi, "exchange", exchange_name)
    commands_.consume_messages(cihazTipi)


def exception_handler(type, value, tb):
    for line in traceback.TracebackException(type, value, tb).format(chain=True):
        logging.exception(line)
    logging.exception(value)
    sys.__excepthook__(type, value, tb)  # calls parent except

# Install exception handler

if __name__ == '__main__':
    sys.excepthook = exception_handler



    app = Flask(__name__)
    @app.route("/")
    def main():
        return jsonify({"main": "hello world"})

    app.add_url_rule('/work', view_func=services_.process)
    app.add_url_rule('/process', methods=["POST"], view_func=services_.processQuene)

    load_dotenv('.env')



    print(platform.system(),sysConfig)

    cx_Oracle.init_oracle_client(lib_dir=sysConfig.get('DB_LIB_DIR', r'/instantclient_19_19'))

    pool = multiprocessing.Pool() # for multitasking

    for r in sysConfig.get('MQ_QUENE', 'TEST1,TEST2').split(','):
        try:
            pool.apply_async(rabbitMQTask, args=(r,))  # for rabbitMQ tasking
        except  Exception as e:
            print ("RABBITMQ: HATA consumer",e.message, e.args)

    app.run(host="0.0.0.0", port=sysConfig.get('PORT', 8080))


