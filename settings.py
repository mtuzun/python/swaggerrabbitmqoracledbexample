import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ["*"]

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': os.getenv('DB_NAME', 'abc'),
        'USER': os.getenv('DB_USERNAME', 'abc'),
        'PASSWORD': os.getenv('DB_PW', 'abc123'),
        'HOST': os.getenv('DB_IP', '192.168.100.1'),
        'PORT': os.getenv('DB_PORT', '1521'),
    }
}

# Update database configuration with $DATABASE_URL.
# noinspection PyBroadException
try:
    import dj_database_url

    db_from_env = dj_database_url.config(conn_max_age=500)
    DATABASES['default'].update(db_from_env)
except:
    pass