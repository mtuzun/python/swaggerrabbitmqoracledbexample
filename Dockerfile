FROM python:3.11.4-slim-bullseye

USER root

# Installing Oracle instant client and gcc
WORKDIR    /opt/oracle
RUN        apt-get update && apt-get install -y libaio1 wget unzip gcc \
            && wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip \
            && unzip instantclient-basiclite-linuxx64.zip \
            && rm -f instantclient-basiclite-linuxx64.zip \
            && cd /opt/oracle/instantclient* \
            && rm -f *jdbc* *occi* *mysql* *README *jar uidrvci genezi adrci \
            && echo /opt/oracle/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf \
            && ldconfig

WORKDIR /home/appuser
RUN cd /home/appuser
COPY . /home/appuser/.
COPY ./instantclient_19_19 /instantclient_19_19
COPY network /opt/oracle/.
COPY . .

ENV PYTHONUNBUFFERED=1 \
    ENVIRONMENT=production
ENV FLASK_ENV=development
ENV ORACLE_HOME=/opt/oracle
ENV PYTHONPATH=/home/appuser
ENV LD_LIBRARY_PATH=/instantclient_19_19

RUN pip install pipenv
RUN pip install -r ./requirements.txt

RUN chmod -R 777 /instantclient_19_19 && chmod -R 777 /tmp && chown -R 185 /home/appuser

EXPOSE 8080
USER 185

CMD [ "python", "./manage.py", "runserver" ]
