import pika
from dotenv import dotenv_values
from json import loads



# .env dosyasını yükle
sysConfig = dotenv_values('.env')

__author__ = 'mtuzun'


class Command:
    def __init__(self, username, password, host, port, virtual_host):
        if username == None:
            return
        self.credentials = pika.PlainCredentials(username, password)
        self.parameters = pika.ConnectionParameters(host, port, virtual_host, self.credentials)
        self.connect()

    def connect(self):
        print(self.parameters)
        self.connection = pika.BlockingConnection(self.parameters)
        self.channel = self.connection.channel()

    def create_exchange(self, exchange_name, exchange_type='topic'):
        self.channel.exchange_declare(exchange=exchange_name, exchange_type=exchange_type,durable=True)

    def create_queue(self, queue_name):
        self.channel.queue_declare(queue=queue_name, durable=True)

    def bind_queue(self, exchange_name, queue_name, routing_key):
        print("exchange_name", exchange_name, "quene_name", queue_name, "route_key", routing_key)
        self.channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=routing_key)

    def publish_message(self, exchange_name, routing_key, message):
        self.channel.basic_publish(exchange=exchange_name, routing_key=routing_key, body=message)

    def consume_messages(self, queue_name):
        self.channel.basic_consume(queue=queue_name, on_message_callback=Command.callBack, auto_ack=True)
        self.channel.start_consuming()

    def close_connection(self):
        if self.connection and self.connection.is_open:
            self.connection.close()

    @classmethod
    def processQuene(self, **kwargs):
        print('RABBITReceived message:', kwargs)

    @classmethod
    def callBack(self, ch, method, properties, body):
        try:
            print('RABBITReceived Quene', method.routing_key, 'Received message:', body.decode())

            self.processQueneDC(kwargs=loads(body.decode()))
        except  Exception as e:
            print("except servis rabbitmq exception:",body.decode(), e.message, e.args)


# RabbitMQ bağlantı bilgileri
username = sysConfig.get('MQ_USERNAME', 'admin')
password = sysConfig.get('MQ_PW', '12345')
host = sysConfig.get('MQ_HOST', 'rabbitmq-service')
port = sysConfig.get('MQ_PORT', '5672')
virtual_host = sysConfig.get('MQ_VIRTUAL_HOST', '/')

commands = Command(username, password, host, port, virtual_host)
