from flask import jsonify ,request
import logging
from sqlalchemy.orm import Session
from .models.init_db import engine
from .models.City import City



__author__ = 'mtuzun'

logger = logging.getLogger(__name__)


class Services(object):

    @classmethod
    def process(cls):

        with Session(engine) as session:
               list = session.query(City).order_by(City.NAME).limit(25).all()

        json_data = [item.to_dict() for item in list]
        return jsonify(json_data)
    @classmethod
    def processQuene(cls):
        try:
            print('Body JSON:',request.json)
            return jsonify({'status':'OK' })
        except Exception as e:
            print('except servis',request.data,e.message, e.args)
            return jsonify(
                {'status': 'HATA'})