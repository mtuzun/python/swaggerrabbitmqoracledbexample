from sqlalchemy import  Column, Integer, String
from .BaseClass import Base
from .init_db import sysConfig
__author__ = "mtuzun"

class City(Base):
    __tablename__ = 'CITY'
    __table_args__ = {'schema': sysConfig.get('DB_NAME','EYS')}

    ID = Column(Integer, primary_key=True)
    NAME = Column(String(200))


    def to_dict(self):

        return {
            'ID': self.ID,
            'NAME': self.NAME,

        }

    @classmethod
    def from_dict(cls, data):
        return cls(
            ID=data.get('ID'),
            NAME=data.get('NAME'),
        )