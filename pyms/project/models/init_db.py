from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import dotenv_values



# .env dosyasını yükle
sysConfig = dotenv_values('.env')


db = SQLAlchemy()
__author__ = "mtuzun"
def getEngine():
    oracle_connection_string='oracle+cx_oracle://{username}:{password}@(DESCRIPTION = (LOAD_BALANCE=on) (FAILOVER=ON) (ADDRESS = (PROTOCOL = TCP)   (HOST = {hostname})(PORT={port})) (CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME={sid})))'


    engine = create_engine(
        oracle_connection_string.format(
            username=sysConfig.get('DB_NAME','abc'),
            password=sysConfig.get('DB_PW','Abc13'),
            hostname=sysConfig.get('DB_IP','192.168.100.1'),
            port=sysConfig.get('DB_PORT','1521'),
            database=sysConfig.get('DB_NAME','ORA'),
            sid=sysConfig.get('DB_SID', 'XE'),
        )
    )
    return engine
engine=getEngine()
def getSession():
    factory = sessionmaker()

    return  factory(bind=engine)


